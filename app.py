from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String
import os
from flask_marshmallow import Marshmallow


app     = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__)) # __file__ means current script
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///' + os.path.join(basedir, 'kubrick.db')
db      = SQLAlchemy(app)
ma      = Marshmallow(app)

# create end points
@app.route('/')
def home():
    return jsonify(data = 'My MLE02 Home Page - Welcome!!')

if __name__=='__main__':
    app.run()